package com.example.kevin.projekttms;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by kevin on 09.04.16.
 */
public class MenuActivity extends AppCompatActivity implements View.OnClickListener  {
    private Button btnDemo;
    private Button btnAktivitaeten;
    private Button btnListDemoData;
    private Button btnListData;
    private Button btnSynchro;
    private Button btnConfig;
    private Button btnClose;
    private Intent intent;
    private DemoDB testDB;
    private DataBase dataBase;
    private ConfigurationSingleton config = ConfigurationSingleton.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_menu);

       config.getStatuses();
        Log.d("info", config.getFilterQuery());
        btnDemo = (Button)findViewById(R.id.btnDemo);
        btnAktivitaeten = (Button)findViewById(R.id.btnAktivitaeten);
        btnSynchro = (Button)findViewById(R.id.btnSynchro);
        btnConfig = (Button)findViewById(R.id.btnConfig);
        btnClose = (Button)findViewById(R.id.btnClose);
        btnListDemoData = (Button)findViewById(R.id.btnListDemo);
        btnListData = (Button)findViewById(R.id.btnAktivitaetenAuflisten);
        btnSynchro = (Button)findViewById(R.id.btnSynchro);

        btnSynchro.setOnClickListener(this);
        btnConfig.setOnClickListener(this);
        btnDemo.setOnClickListener(this);
        btnAktivitaeten.setOnClickListener(this);
        btnClose.setOnClickListener(this);
        btnListDemoData.setOnClickListener(this);
        btnListData.setOnClickListener(this);

        testDB = new DemoDB(this, "testDaten");
        dataBase = new DataBase(this, "daten");


       if  (dataBase.getDbLenght(config.getFilterQuery()) == 0)  {
            btnAktivitaeten.setEnabled(false);
            btnListData.setEnabled(false);
         }  else  {
           btnAktivitaeten.setEnabled(true);
           btnListData.setEnabled(true);
           // dataBase.generateObjectInstances(config.getFilterQuery());
         }
        if (testDB.getDbLenght(config.getFilterQuery()) == 0) {
            testDB.insertDemoData();
            testDB.generateObjectInstances(config.getFilterQuery());
         }   else  {
            testDB.generateObjectInstances(config.getFilterQuery());
         }
     }

    @Override
    public void onClick(View v)  {
        if (v == btnDemo) {
            intent = new Intent(this, MapsActivity.class);
            intent.putExtra("status", 1);
            startActivity(intent);
         }   else  if (v == btnAktivitaeten) {
            intent = new Intent(this, MapsActivity.class);
            intent.putExtra("status", 2);
            startActivity(intent);
         }   else  if (v == btnListData) {
            intent = new Intent(this, DataListActivity.class);
            intent.putExtra("status", 2);
            startActivity(intent);
         }   else  if (v == btnListDemoData) {
            intent = new Intent(this, DataListActivity.class);
            intent.putExtra("status", 1);
            startActivity(intent);
         }   else  if  (v == btnClose)  {
                Toast.makeText(this, "Auf wiedersehen!", Toast.LENGTH_SHORT).show();
                intent = new Intent(this, WelcomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
         }   else  if (v == btnConfig) {
            intent = new Intent(this, ConfigurationActivity.class);
            startActivity(intent);
         }   else  if (v == btnSynchro) {

            if (checkForConnection() == true) {
            intent = new Intent(this, DownloadXmlFileActivity.class);
            startActivity(intent); }
             else  {
                Toast.makeText(this, "Es besteht keine Internetverbindung", Toast.LENGTH_LONG).show();

             }
         }

     }


    private boolean checkForConnection() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
     }

 }
