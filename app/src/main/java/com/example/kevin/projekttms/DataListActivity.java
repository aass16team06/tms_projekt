package com.example.kevin.projekttms;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

/**
 * Created by kevin on 14.04.16.
 */
public class DataListActivity extends Activity implements View.OnClickListener, FragmentSettings.RefreshCallbackSwitch {
    private TableLayout tableLayout;
    private TableRow[] tableRows;
    private TextView[] textViews;
    private DemoDB db;
    private DataBase db1;
    private float textSize = 20;
    private Button btnBackToMenu;
    private Intent intent;
    private ConfigurationSingleton config = ConfigurationSingleton.getInstance();
    private Bundle data;
    private int colorChange = 0;
    private int status;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        boolean state = isTablet(this);
        if (state == true) {
            setContentView(R.layout.content_list_tablet);
            android.app.FragmentManager fm = getFragmentManager();
            android.app.FragmentTransaction transaction = fm.beginTransaction();
            FragmentSettings settings = new FragmentSettings();
            transaction.add(R.id.frame_rechts, settings, FragmentSettings.TAG);
            transaction.commit();

         }   else  {
            setContentView(R.layout.content_list);
         }
        tableLayout = (TableLayout)findViewById(R.id.listTableLayout);
        intent = getIntent();
        data = intent.getExtras();
        status = data.getInt("status");
        btnBackToMenu = (Button)findViewById(R.id.backToMenuFromList);
        btnBackToMenu.setOnClickListener(this);

        if  (status == 2) {
            db1 = new DataBase(this, "daten");
            createRows();
         }  else   {
            db = new DemoDB(this, "testDaten");
            createRowsDemo();
         }

       /* View v = this.findViewById(R.id.linearLayout);
        Log.d(getClass().getSimpleName(), String.valueOf(v.getTag()));

        if (v != null) {
            Log.d("info", "adding view View created");
            android.app.FragmentManager fm = getFragmentManager();
            android.app.FragmentTransaction transaction = fm.beginTransaction();
            FragmentSettings settings = new FragmentSettings();
            transaction.add(R.id.frame_rechts, settings, FragmentSettings.TAG);
            transaction.commit();
         } */





     }

    public static boolean isTablet(Context ctx) {
        return (ctx.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE;
     }


    private void createRows() {


        db1.generateObjectInstances(config.getFilterQuery());

        int lenght = db1.getDbLenght(config.getFilterQuery());
        tableRows = new TableRow[lenght];
        textViews = new TextView[lenght];
        for (int i = 0; i < lenght; i++) {
         tableRows[i] = new TableRow(this);
         tableRows[i].setOnClickListener(this);
         textViews[i] = new TextView(this);
         textViews[i].setTextSize(textSize);
         textViews[i].setText(db1.orte[i].getEinrichtung());
         tableRows[i].addView(textViews[i]);

             if  (colorChange == 0) {
         tableRows[i].setBackgroundColor(Color.rgb(255, 178,108));
            colorChange++; }
             else   {
                tableRows[i].setBackgroundColor(Color.rgb(204, 178, 148));
                colorChange = 0;
             }

         tableRows[i].setId(i);
         tableLayout.addView(tableRows[i]);
         }
     }

    private void createRowsDemo() {
        db.generateObjectInstances(config.getFilterQuery());

        int lenght = db.getDbLenght(config.getFilterQuery());
        tableRows = new TableRow[lenght];
        textViews = new TextView[lenght];
        for (int i = 0; i < lenght; i++) {
            tableRows[i] = new TableRow(this);
            tableRows[i].setOnClickListener(this);
            textViews[i] = new TextView(this);
            textViews[i].setTextSize(textSize);
            textViews[i].setText(db.orte[i].getEinrichtung());
            tableRows[i].addView(textViews[i]);

            if (colorChange == 0) {
                tableRows[i].setBackgroundColor(Color.rgb(255, 178,108));
                colorChange++;
             }
             else   {
                tableRows[i].setBackgroundColor(Color.rgb(204, 178, 148));
                colorChange = 0;
             }

            tableRows[i].setId(i);
            tableLayout.addView(tableRows[i]);
         }
     }


    @Override
    public void onClick(View v)  {
        if (v == btnBackToMenu) {
            intent = new Intent(this, MenuActivity.class);
            startActivity(intent);
         }  else
        intent = new Intent(this, SingleViewActivity.class);
        intent.putExtra("ViewElement", v.getId());
        intent.putExtra("statusFromMapView", status);
        intent.putExtra("source", "list");
        intent.putExtra("status", status);
        startActivity(intent);
     }

    @Override
    public void refresh()  {
        while(tableLayout.getChildCount() > 1) {
            tableLayout.removeViewAt(1);
         }
        if (status == 2) {
            createRows();
         }  else  {
            createRowsDemo();
         }

     }


 }
