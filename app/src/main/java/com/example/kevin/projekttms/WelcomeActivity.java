package com.example.kevin.projekttms;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


public class WelcomeActivity extends AppCompatActivity implements View.OnTouchListener  {

    TextView blinkView;
    Animation anim;
    ImageView iconView;
    RelativeLayout mainLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_welcome);
        iconView = (ImageView) findViewById(R.id.touchIcon);
        blinkView = (TextView) findViewById(R.id.BeruehrenView);
        mainLayout = (RelativeLayout) findViewById(R.id.LayoutTouch);
        anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(1000);
        anim.setStartOffset(20);
        anim.setRepeatCount(Animation.INFINITE);
        anim.setRepeatMode(Animation.REVERSE);
        iconView.startAnimation(anim);
        blinkView.startAnimation(anim);
        mainLayout.setOnTouchListener(this);

     }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        Log.d("info", "activity wechseln");
        Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
        return true;
     }















 }
