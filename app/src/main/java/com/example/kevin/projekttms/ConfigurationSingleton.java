package com.example.kevin.projekttms;

import android.content.Context;
import android.location.Geocoder;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by kevin on 18.04.16.
 */
public class ConfigurationSingleton  {

    private boolean leseorte = true;
    private boolean kindertagesstaette = true;
    private boolean familienzentren = true;
    private boolean jugendeinrichtungen = true;
    private String filterQuery = "";
    private final int anzahlArten = 4;
    private ArtObjekt[] listeDerArten;


    private static ConfigurationSingleton ourInstance = new ConfigurationSingleton();

    public static ConfigurationSingleton getInstance()  {
        return ourInstance;
     }

    private ConfigurationSingleton()  {
     listeDerArten = new ArtObjekt[anzahlArten];
     }

    public void setFamilienzentren(boolean familienzentren) {
        this.familienzentren = familienzentren;
     }

    public void setJugendeinrichtungen(boolean jugendeinrichtungen)  {
        this.jugendeinrichtungen = jugendeinrichtungen;
     }

    public void setKindertagesstaette(boolean kindertagesstaette)  {
        this.kindertagesstaette = kindertagesstaette;
     }

    public void setLeseorte(boolean leseorte)  {
        this.leseorte = leseorte;
     }

    public boolean isFamilienzentren()  {
        return familienzentren;
     }

    public boolean isJugendeinrichtungen()  {
        return jugendeinrichtungen;
     }

    public boolean isKindertagesstaette()  {
        return kindertagesstaette;
     }

    public boolean isLeseorte()  {
        return leseorte;
     }

    public void getStatuses() {
        Log.d("info", "switch1: " + Boolean.toString(leseorte) +
                " switch2: " + Boolean.toString(kindertagesstaette) +
                " switch3: " + Boolean.toString(familienzentren) +
                " switch4: " + Boolean.toString(jugendeinrichtungen)
        );
     }

    public String getFilterQuery() {
        filterQuery = "";
    listeDerArten[0] = new ArtObjekt(leseorte, "Leseorte");
    listeDerArten[1] = new ArtObjekt(kindertagesstaette, "Kindertagesstätte");
    listeDerArten[2] = new ArtObjekt(familienzentren, "Familienzentrum");
    listeDerArten[3] = new ArtObjekt(jugendeinrichtungen, "Jugendeinrichtung");

        for(int i = 0; i < listeDerArten.length; i ++) {
         if (listeDerArten[i].isState() == true) {
             filterQuery += "art = '" + listeDerArten[i].getName() + "' OR ";
          }


         }
        if (filterQuery.endsWith("OR ")) {
            filterQuery = filterQuery.substring(0, filterQuery.lastIndexOf(" OR "));
         }
        Log.d("info", filterQuery);
        //filterQuery = "art = 'Leseorte' OR art = 'Familienzentrum'";
        return filterQuery;
     }

 }
