package com.example.kevin.projekttms;

/**
 * Created by kevin on 20.04.16.
 * Klasse für die Einstellungen
 */
public class ArtObjekt  {
    private boolean state;
    private String name;

    public ArtObjekt(boolean state, String name ) {
        this.state = state;
        this.name = name;
     }


    public boolean isState()  {
        return state;
     }

    public String getName()  {
        return name;
     }
 }
