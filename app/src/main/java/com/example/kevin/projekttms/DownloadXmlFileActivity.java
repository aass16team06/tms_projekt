package com.example.kevin.projekttms;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.io.File;
import java.io.InputStream;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by kevin on 22.04.16.
 */
public class DownloadXmlFileActivity extends AppCompatActivity implements View.OnClickListener  {
    private DownloadManager downloadManager;
    private String downloadPath = "http://www.berlin.de/ba-marzahn-hellersdorf/" +
            "politik-und-verwaltung/aemter/jugendamt/einrichtungssuche/index.php/index/all.xml?q=";
    private Uri uri;
    private final String path = "/storage/emulated/0/Download/XmlRessources.xml";
    private File file;
    private InputStream inputStream;
    private XmlParser xmlParser;
    private Ort[] orte;
    private Geocoder geocoder;
    private AsyncGeocooding asyncGeocooding;
    private Intent intent;
    private Button btnBack;
    private DataBase dataBase;
    private ProgressBar progressBar;
    private Timer timer;
    private TimerTask timerTask;
    private Handler handler;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState)  {
        handler = new Handler(this.getMainLooper());

        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_synchronisation);
      /*  timer = new Timer("timer");
        timerTask = new TimerTask() {
          @Override
            public void run() {
              Log.d("info", asyncGeocooding.getStatus().toString());
              if  (asyncGeocooding.getStatus().toString().equalsIgnoreCase("FINISHED"))  {
                  timerTask.cancel();
                  timer.cancel();
               }
           }
         } ;*/




        btnBack = (Button) findViewById(R.id.btnBackToMenuFromSynchro);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);
        btnBack.setOnClickListener(this);
        btnBack.setEnabled(false);
        geocoder = new Geocoder(this);
        checkifFileExists();
        downloadXmlFile();
        checkifDataBaseExists();
        xmlParser = new XmlParser();
        orte = xmlParser.getOrte();
        asyncGeocooding = new AsyncGeocooding(this, orte);
        asyncGeocooding.execute();
        handler.postDelayed(runnable, 10000);
        //timer.schedule(timerTask, 10000, 3000);

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
     }

    Runnable runnable = new Runnable()  {
        @Override
        public void run()  {
            Log.d("info", asyncGeocooding.getStatus().toString());
            if  (asyncGeocooding.getStatus().toString().equalsIgnoreCase("FINISHED"))  {
                progressBar.setVisibility(View.INVISIBLE);
                btnBack.setEnabled(true);
             }  else  {
                handler.postDelayed(runnable, 3000);
             }
         }
     } ;


    private void stopProgressBar() {
        this.progressBar.setVisibility(View.INVISIBLE);
        this.btnBack.setEnabled(true);
     }


    private void checkifFileExists()  {
        file = new File(path);
        Log.d("info", "file existiert: " + Boolean.toString(file.exists()));
        if  (file.exists() == true)  {
            file.delete();
         }
     }

    private void checkifDataBaseExists()  {
        dataBase = new DataBase(this, "daten");
        if  (dataBase.getDbLenght(null) > 0)  {
            Log.d("info", "loschen der daten");
            dataBase.delete();
         }   else   {
            Log.d("info", "nicht loschen der daten");
         }
     }


    private void downloadXmlFile()  {
        downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        uri = Uri.parse(downloadPath);
        DownloadManager.Request request = new DownloadManager.Request(uri);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "XmlRessources.xml");
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
        downloadManager.enqueue(request);
        while (file.exists() == false)  {
            SystemClock.sleep(1500);
            Log.d("info", "download xmlfile: file existiert: " + Boolean.toString(file.exists()));
         }
     }


    @Override
    public void onClick(View v)  {
        intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
     }

    @Override
    public void onStart()  {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "DownloadXmlFile Page", // TODO: Define a title for the content shown.
                // TODO: if  you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.example.kevin.projekttms/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
     }

    @Override
    public void onStop()  {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "DownloadXmlFile Page", // TODO: Define a title for the content shown.
                // TODO: if  you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.example.kevin.projekttms/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
     }
 }
