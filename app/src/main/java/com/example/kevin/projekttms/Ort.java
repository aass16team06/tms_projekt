package com.example.kevin.projekttms;

/**
 * Created by kevin on 12.04.16.
 */
public class Ort  {
    private int id;
    private String art;
    private String region;
    private String einrichtung;
    private String anschrift;
    private String ort;
    private String homePage;
    private String telefonNr;
    private double breitenGrad;
    private double laengenGrad;


    public Ort() {
     }





    public void setId(int id) {
        this.id = id;
     }

    public void setArt(String art)  {
        this.art = art;
     }

    public void setanschrift(String anschrift)  {
        this.anschrift = anschrift;
     }

    public void setTelefonNr(String telefonNr)  {
        this.telefonNr = telefonNr;
     }

    public void setRegion(String region)  {
        this.region = region;
     }

    public void setOrt(String ort)  {
        this.ort = ort;
     }

    public void setLaengenGrad(double laengenGrad)  {
        this.laengenGrad = laengenGrad;
     }

    public void setHomePage(String homePage)  {
        this.homePage = homePage;
     }

    public void setEinrichtung(String einrichtung)  {
        this.einrichtung = einrichtung;
     }

    public void setBreitenGrad(double breitenGrad)  {
        this.breitenGrad = breitenGrad;
     }

    public double getBreitenGrad()  {
        return breitenGrad;
     }

    public double getLaengenGrad()  {
        return laengenGrad;
     }

    public String getArt()  {
        return art;
     }

    public String getEinrichtung() {   return einrichtung; }

    public String getanschrift() {   return anschrift; }

    public int getId()  {
        return id;
     }

    public String getHomePage()  {
        return homePage;
     }

    public String getOrt()  {
        return ort;
     }

    public String getRegion()  {
        return region;
     }

    public String getTelefonNr()  {
        return telefonNr;
     }



    public String toString() {
        return  "ID: " + Integer.toString(id) + " Art: " + art + " anschrift: " + anschrift +
                " Telefon: " + telefonNr +
                " Region: " + region + " Ort: " + ort + " Homepage: " + homePage;
     }
 }
