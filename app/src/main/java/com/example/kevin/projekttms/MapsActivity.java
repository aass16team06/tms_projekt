package com.example.kevin.projekttms;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, View.OnClickListener, LocationListener, GoogleMap.OnMarkerClickListener  {

    private GoogleMap mMap;
    private Button btnBack;
    private Button btnLocate;
    private int status;
    private Bundle data;
    private Intent intent;
    private double lat;
    private double lng;
    private LocationManager locationManager;
    private DemoDB testDB;
    private ConfigurationSingleton config = ConfigurationSingleton.getInstance();
    private int loadStatus = 0;
    private static final LatLng Berlin = new LatLng(52.520007, 13.404953999999975);
    private DataBase dataBase;


    @Override
    protected void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo_maps);
        intent = getIntent();
        data = intent.getExtras();
        status = data.getInt("status");
        dataBase = new DataBase(this, "daten");
        testDB = new DemoDB(this, "testDaten");
        Log.d("status ist: ", Integer.toString(status));
        btnBack = (Button) findViewById(R.id.backToMenu);
        btnBack.setOnClickListener(this);
        btnLocate = (Button) findViewById(R.id.locateMe);
        btnLocate.setOnClickListener(this);
        // Obtain the SupportMapFragment and get notif ied when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

     }

    @Override
    protected void onPause()  {
        super.onPause();

     }

    private void insertDemoPoints() {
        testDB.generateObjectInstances(config.getFilterQuery());
        for(int i = 0; i<testDB.getDbLenght(config.getFilterQuery()); i++) {
            LatLng point = new LatLng(testDB.orte[i].getBreitenGrad(), testDB.orte[i].getLaengenGrad());
            mMap.addMarker(new MarkerOptions().position(point).title(testDB.orte[i].getArt()));

         }

     }

    private void insertPoints() {
        dataBase.generateObjectInstances(config.getFilterQuery());
        for(int i = 0; i<dataBase.getDbLenght(config.getFilterQuery()); i++) {
            LatLng point = new LatLng(dataBase.orte[i].getBreitenGrad(), dataBase.orte[i].getLaengenGrad());
            mMap.addMarker(new MarkerOptions().position(point).title(dataBase.orte[i].getArt()));

         }

     }

    @Override
    protected void onResume()  {
        super.onResume();
     }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * if  Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap)  {
        mMap = googleMap;
        mMap.setOnMarkerClickListener(this);

        // Move the camera to show the marker.
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Berlin, 9));

        if (loadStatus==0) {

        if (status == 1) {
            insertDemoPoints();
            loadStatus = 1;
         }  else  if (status == 2) {
            insertPoints();
            loadStatus = 1;

         }  else  if (status == 3) {
            String db;
            int elem;
            data = intent.getExtras();
            db = data.getString("database");
            elem = data.getInt("element");
            if (db.equalsIgnoreCase("data") == true) {
                dataBase.generateObjectInstances(config.getFilterQuery());
                LatLng point = new LatLng(dataBase.orte[elem].getBreitenGrad(), dataBase.orte[elem].getLaengenGrad());
                mMap.addMarker(new MarkerOptions().position(point).title(dataBase.orte[elem].getArt()));
             }  else  {
            testDB.generateObjectInstances(config.getFilterQuery());
            LatLng point = new LatLng(testDB.orte[elem].getBreitenGrad(), testDB.orte[elem].getLaengenGrad());
            mMap.addMarker(new MarkerOptions().position(point).title(testDB.orte[elem].getArt()));
             }
         }
        loadStatus = 1;
         }


        if  (lat == 0 && lng == 0)  {

         }   else   {
            LatLng point = new LatLng(lat, lng);
            mMap.addMarker(new MarkerOptions().position(point).title("Ich"));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(point, 5));
         }


     }

    @Override
    public void onClick(View v)  {
        if  (v == btnBack)  {
            Intent intent = new Intent(this, MenuActivity.class);
            startActivity(intent);
         }   else  if  (v == btnLocate)  {
            locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) == true) {

            if  (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED)  {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
             }
                locationManager.requestSingleUpdate(locationManager.GPS_PROVIDER, this, Looper.myLooper());
             }  else
                Toast.makeText(this, "Schalten sie bitte ihre GPS Funktion ein", Toast.LENGTH_LONG).show();

         }
     }

    @Override
    public void onLocationChanged(Location location)  {
    lat = location.getLatitude();
    lng = location.getLongitude();
    Log.d("Koordinaten" , Double.toString(lat) + " " + Double.toString(lng));
        onMapReady(mMap);
     }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras)  {

     }

    @Override
    public void onProviderEnabled(String provider)  {

     }

    @Override
    public void onProviderDisabled(String provider)  {

     }

    @Override
    public boolean onMarkerClick(Marker marker)  {
        Log.d("info", "marker angeclickt " + marker.getId().substring(1,2) );
        if (marker.getTitle().equalsIgnoreCase("ich")) {
            Toast.makeText(this, "Zu der eigenen position existieren keine Informationen", Toast.LENGTH_LONG).show();
         }  else   {
        Intent intent = new Intent(this, SingleViewActivity.class);
        intent.putExtra("ViewElement", Integer.valueOf(marker.getId().substring(1,2)));
        intent.putExtra("source", "map");
        intent.putExtra("status", status);
        startActivity(intent);

         }
        return false;
     }
 }
