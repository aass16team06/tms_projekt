package com.example.kevin.projekttms;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.File;

/**
 * Created by kevin on 24.04.16.
 */
public class DataBase extends SQLiteOpenHelper  {
    public Ort[] orte;
    private Cursor cursor;
    private SQLiteDatabase db;
    private ContentValues daten;
    File file = new File("/data/data/com.example.kevin.projekttms/databases/daten");


    public DataBase(Context activity, String dbName) {
        super(activity, dbName, null, 1);
        db = getWritableDatabase();
     }


    public void delete() {
        boolean suc;
        suc = db.deleteDatabase(file);
        Log.d("info", Boolean.toString(suc));
     }



    @Override
    public void onCreate(SQLiteDatabase db)  {
        try  {
            String sql = "CREATE TABLE orte " +
                    "(id INTEGER PRIMARY KEY, " +
                    "art VARCHAR(20) NOT NULL, " +
                    "region VARCHAR(20) NOT NULL, " +
                    "einrichtung VARCHAR(20) NOT NULL, " +
                    "anschrift VARCHAR(20) NOT NULL, " +
                    "ort VARCHAR(20) NOT NULL, " +
                    "homepage VARCHAR(50) NOT NULL, " +
                    "telefonnr VARCHAR(30) NOT NULL, " +
                    "breitengrad REAL, " +
                    "laengengrad REAL)";
            db.execSQL(sql);
            Log.d("info", "Db erstellt" + db.getPath());
         }  catch(Exception e) {
            e.printStackTrace();
         }
     }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)  {
     }

    public void insertData(Ort[] newOrte) {
        daten = new ContentValues();
        for(int i = 0; i < newOrte.length; i++) {
            daten = new ContentValues();
            daten.put("id", newOrte[i].getId());
            daten.put("art",newOrte[i].getArt() );
            daten.put("region", newOrte[i].getRegion());
            daten.put("einrichtung", newOrte[i].getEinrichtung());
            daten.put("anschrift", newOrte[i].getanschrift());
            daten.put("ort", newOrte[i].getOrt());
            daten.put("homepage", newOrte[i].getHomePage());
            daten.put("telefonnr", newOrte[i].getTelefonNr());
            daten.put("breitengrad", newOrte[i].getBreitenGrad());
            daten.put("laengengrad", newOrte[i].getLaengenGrad());
            db.insert("orte", null, daten);
            daten.clear();


         }
     }



    @Override
    public synchronized void close() {
        if (db!=null) {
            db.close();
            db = null;
         }
        super.close();
     }

    public void generateObjectInstances(String queryFilter) {
        cursor = db.query("orte", null, queryFilter, null, null, null, null);
        int anzahl = cursor.getCount();
        orte = new Ort[anzahl];
        cursor.moveToFirst();
        for(int i = 0; i < anzahl; i++) {
            orte[i] = new Ort();
            orte[i].setId(cursor.getInt(0));
            orte[i].setArt(cursor.getString(1));
            orte[i].setRegion(cursor.getString(2));
            orte[i].setEinrichtung(cursor.getString(3));
            orte[i].setanschrift(cursor.getString(4));
            orte[i].setOrt(cursor.getString(5));
            orte[i].setHomePage(cursor.getString(6));
            orte[i].setTelefonNr(cursor.getString(7));
            orte[i].setBreitenGrad(cursor.getDouble(8));
            orte[i].setLaengenGrad(cursor.getDouble(9));
            cursor.moveToNext();
         }
        cursor.close();
     }

    public int getDbLenght(String queryFilter) {
        cursor = db.query("orte", null, queryFilter, null, null, null, null);
        int anzahl = cursor.getCount();
        cursor.close();
        return anzahl;
     }




 }
