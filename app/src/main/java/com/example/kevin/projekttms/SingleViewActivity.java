package com.example.kevin.projekttms;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by kevin on 15.04.16.
 */
public class SingleViewActivity extends AppCompatActivity implements View.OnClickListener  {
    private TextView view1;
    private TextView view2;
    private TextView view3;
    private TextView view4;
    private TextView view5;
    private Intent intent;
    private Bundle data;
    private int statusFromMapView;
    private int status;
    private int element;
    private DemoDB testDb;
    private DataBase dataBase;
    private Button btnBackToMenu;
    private Button btnShowOnMap;
    private Button btnBackToMap;
    private ConfigurationSingleton config = ConfigurationSingleton.getInstance();
    private String source;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.content_single_view);
    btnBackToMenu = (Button)findViewById(R.id.btnBackToListFromSingleView);
    btnBackToMenu.setOnClickListener(this);
    btnShowOnMap = (Button)findViewById(R.id.btnSingleViewAufMapAnzeigen);
    btnShowOnMap.setOnClickListener(this);
    btnBackToMap = (Button)findViewById(R.id.btnBackToMapFromSingleView);
    btnBackToMap.setEnabled(false);
    btnBackToMap.setVisibility(View.INVISIBLE);
    btnBackToMap.setOnClickListener(this);

    //testDb = new DemoDB(this, "testDaten");
    //testDb.generateObjectInstances(config.getFilterQuery());
    view1 = (TextView)findViewById(R.id.singleViewText1);
    view2 = (TextView)findViewById(R.id.singleViewText2);
    view3 = (TextView)findViewById(R.id.singleViewText3);
    view4 = (TextView)findViewById(R.id.singleViewText4);
    view5 = (TextView)findViewById(R.id.singleViewText5);
    intent = getIntent();
    data = intent.getExtras();
    element = data.getInt("ViewElement");
    status = data.getInt("status");
        source = data.getString("source");
        statusFromMapView = data.getInt("statusFromMapView");
   /* if (statusFromMapView == 1) {
        btnBackToMap.setVisibility(View.VISIBLE);
        btnBackToMap.setEnabled(true);
        btnShowOnMap.setEnabled(false);
        btnShowOnMap.setVisibility(View.INVISIBLE);
     }
         else  if (statusFromMapView == 2) {

     }
    view.setText(testDb.orte[element].getArt() + "  " +  testDb.orte[element].getEinrichtung());
*/
        if (source.equalsIgnoreCase("map") == true) {
            btnBackToMap.setVisibility(View.VISIBLE);
            btnBackToMap.setEnabled(true);
            btnShowOnMap.setEnabled(false);
            btnShowOnMap.setVisibility(View.INVISIBLE);
            btnBackToMenu.setEnabled(false);
            btnBackToMenu.setVisibility(View.INVISIBLE);
            if (status == 2) {
                dataBase = new DataBase(this, "daten");
                dataBase.generateObjectInstances(config.getFilterQuery());
                insertData();
             }  else  {
                testDb = new DemoDB(this, "testDaten");
                testDb.generateObjectInstances(config.getFilterQuery());
                insertDataDemo();
              }
         }  else  {
            if (status == 2) {
                dataBase = new DataBase(this, "daten");
                dataBase.generateObjectInstances(config.getFilterQuery());
                insertData();
             }  else  {
                testDb = new DemoDB(this, "testDaten");
                testDb.generateObjectInstances(config.getFilterQuery());
                insertDataDemo();
             }

         }

     }

    private void insertDataDemo() {
        view1.setText(testDb.orte[element].getArt() + "  " +  testDb.orte[element].getEinrichtung());
        view2.setText(testDb.orte[element].getRegion());
        view3.setText(testDb.orte[element].getanschrift() + "  " +  testDb.orte[element].getOrt());
        view4.setText(testDb.orte[element].getTelefonNr());
        view5.setText(testDb.orte[element].getHomePage());


     }

    private void insertData() {
        view1.setText(dataBase.orte[element].getArt() + "  " +  dataBase.orte[element].getEinrichtung());
        view2.setText(dataBase.orte[element].getRegion());
        view3.setText(dataBase.orte[element].getanschrift() + " " + dataBase.orte[element].getOrt());
        view4.setText(dataBase.orte[element].getTelefonNr());
        view5.setText(dataBase.orte[element].getHomePage());


     }


    @Override
    public void onClick(View v)  {
        if (v==btnBackToMenu) {
            intent = new Intent(this, DataListActivity.class);
            intent.putExtra("status", status);
            startActivity(intent);
         }   else  if (v == btnShowOnMap || v == btnBackToMap) {
            //Log.d("status in der singleview ist", Integer.toString(status));
            intent = new Intent(this, MapsActivity.class);
            intent.putExtra("status", 3);
            intent.putExtra("element", element);
            if (status == 2) {
            intent.putExtra("database", "data");
             }  else  {
             intent.putExtra("database", "demoData");
             }
            startActivity(intent);
         }  /* else  if (v == btnBackToMap) {
            intent = new Intent(this, MapsActivity.class);
            intent.putExtra("status", status);
            startActivity(intent);
         } */
     }
 }
