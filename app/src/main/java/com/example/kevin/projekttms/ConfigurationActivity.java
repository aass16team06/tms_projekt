package com.example.kevin.projekttms;

import android.content.Intent;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;

/**
 * Created by kevin on 18.04.16.
 */
public class ConfigurationActivity extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener  {
    private Button btnBackToMenu;
    private Intent intent;
    private Switch switch1;
    private Switch switch2;
    private Switch switch3;
    private Switch switch4;
    private ConfigurationSingleton config = ConfigurationSingleton.getInstance();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_configuration);
        btnBackToMenu = (Button)findViewById(R.id.btnBackToMenuFromConfig);
        btnBackToMenu.setOnClickListener(this);
        switch1 = (Switch)findViewById(R.id.switch1);
        switch1.setOnCheckedChangeListener(this);
        switch1.setChecked(config.isLeseorte());
        switch2 = (Switch)findViewById(R.id.switch2);
        switch2.setOnCheckedChangeListener(this);
        switch2.setChecked(config.isKindertagesstaette());
        switch3 = (Switch)findViewById(R.id.switch3);
        switch3.setOnCheckedChangeListener(this);
        switch3.setChecked(config.isFamilienzentren());
        switch4 = (Switch)findViewById(R.id.switch4);
        switch4.setOnCheckedChangeListener(this);
        switch4.setChecked(config.isJugendeinrichtungen());

        config.getStatuses();
     }


    @Override
    public void onClick(View v)  {
        intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
     }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)  {
        if (buttonView == switch1) {
            Log.d("info switch1", Boolean.toString(switch1.isChecked()));
            config.setLeseorte(switch1.isChecked());
         }  else  if (buttonView == switch2) {
            Log.d("info switch2", Boolean.toString(switch2.isChecked()));
            config.setKindertagesstaette(switch2.isChecked());
         }  else  if (buttonView == switch3) {
            Log.d("info switch3", Boolean.toString(switch3.isChecked()));
            config.setFamilienzentren(switch3.isChecked());
         }  else  if (buttonView == switch4) {
            Log.d("info switch4", Boolean.toString(switch4.isChecked()));
            config.setJugendeinrichtungen(switch4.isChecked());

         }

     }
 }
