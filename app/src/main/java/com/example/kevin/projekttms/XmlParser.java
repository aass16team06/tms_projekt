package com.example.kevin.projekttms;


import android.util.Log;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

/**
 * @author kevin Wienzek - kevinwienzek94@yahoo.de
 * Klasse, die die Objekte aus der XML-Datei ausliest und sie instanziert
 */
public class XmlParser  {
    private String xmlPfad = "/storage/emulated/0/Download/XmlRessources.xml";
    private File inputFile;
    private Ort[] orte;

    /**
     * Klasse erhaelt Pfad zu einer XML-Datei, die geparsed werden soll
     *  Pfad der Datei
     */
    public XmlParser() {
     xmlParse();
     }

    public XmlParser(String xmlPfad) {
        this.xmlPfad = xmlPfad;
        xmlParse();
     }


    /**
     * Methode liest die XML-Datei ein und extrahiert die notwendigen Informationen, um daraus Objekte zu instanziieren
     * @return gibt ein Array von Raeumen zurueck
     */
    public void xmlParse() {
        try  {
            NodeList nodeList;
            Node node;
            Log.d("info", "im parser drin");
            inputFile = new File(xmlPfad);
            Log.d("info", Boolean.toString(inputFile.exists()));
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();
            Log.d("info", "angekommen");
            NodeList activityList = doc.getElementsByTagName("item"); // Liste von Nodes wird instaziiert, fuer jeden "room" Tag gibt es einen Wert
            Log.d("info", Integer.toString(activityList.getLength()));
            orte = new Ort[activityList.getLength()-1];
            for (int temp = 0; temp < activityList.getLength()-1; temp++)  {
                orte[temp] = new Ort();
                Node activityNode = activityList.item(temp); // die einzelnen Werte aus der NodeList werden entnommen und die noetigen Attributwerte herausgelesen
                if  (activityNode.getNodeType() == Node.ELEMENT_NODE)  {
                    Element activityElement = (Element) activityNode; // Node wird in ein Element-typ gecastet, um die noetigen Informationen hinauszufiltern
                    Log.d("info", "original id " + activityElement.getAttribute("original"));
                    orte[temp].setId(Integer.parseInt(activityElement.getAttribute("original")));
                     nodeList = activityElement.getElementsByTagName("art");
                     node = nodeList.item(0);
                     orte[temp].setArt(node.getTextContent());
                     nodeList = activityElement.getElementsByTagName("anschrift");
                    node = nodeList.item(0);
                    orte[temp].setanschrift(node.getTextContent());
                     nodeList = activityElement.getElementsByTagName("telefon");
                    node = nodeList.item(0);
                    orte[temp].setTelefonNr(node.getTextContent());
                     nodeList = activityElement.getElementsByTagName("region");
                    node = nodeList.item(0);
                    orte[temp].setRegion(node.getTextContent());
                     nodeList = activityElement.getElementsByTagName("ort");
                    node = nodeList.item(0);
                    orte[temp].setOrt(node.getTextContent());
                     nodeList = activityElement.getElementsByTagName("einrichtung");
                    node = nodeList.item(0);
                    orte[temp].setEinrichtung(node.getTextContent());
                     nodeList = activityElement.getElementsByTagName("homepage");
                    node = nodeList.item(0);
                    orte[temp].setHomePage(node.getTextContent());

             }  }

            for(int i = 0; i<activityList.getLength()-1; i++) {
                Log.d("info", orte[i].toString());
             }

         }  catch (Exception e)  {
            e.printStackTrace();
         }
     }

    /**
     * Methode instanziert die einzelnen "Room" Objekte
     */
    public Ort[] getOrte() {
       return orte;
     }





 }
