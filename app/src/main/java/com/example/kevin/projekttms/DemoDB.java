package com.example.kevin.projekttms;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.File;

/**
 * Created by kevin on 12.04.16.
 */
public class DemoDB extends SQLiteOpenHelper  {
    ContentValues daten;
    public Ort[] orte;
    private Cursor cursor;
    private SQLiteDatabase db;
    File file = new File("/data/data/com.example.kevin.projekttms/databases/testDaten");

    public DemoDB(Context activity, String dbName) {
        super(activity, dbName, null, 1);
        db = getWritableDatabase();
     }


    public void getPath() {
        Log.d("db path", db.getPath());
     }

    @Override
    public void onCreate(SQLiteDatabase db)  {
        try  {
            String sql = "CREATE TABLE orte " +
                    "(id INTEGER PRIMARY KEY, " +
                    "art VARCHAR(20) NOT NULL, " +
                    "region VARCHAR(20) NOT NULL, " +
                    "einrichtung VARCHAR(20) NOT NULL, " +
                    "anschrift VARCHAR(20) NOT NULL, " +
                    "ort VARCHAR(20) NOT NULL, " +
                    "homepage VARCHAR(50) NOT NULL, " +
                    "telefonnr VARCHAR(30) NOT NULL, " +
                    "breitengrad REAL, " +
                    "laengengrad REAL)";
            db.execSQL(sql);
            Log.d("info", "Db erstellt" + db.getPath());
         }  catch(Exception e) {
            e.printStackTrace();
         }

     }

    public void insertDemoData() {
        Log.d("info", "creating data");
        long id;
        daten = new ContentValues();
        daten.put("id", 1);
        daten.put("art", "Leseorte");
        daten.put("region", "Hellersdorf-Nord");
        daten.put("einrichtung", "Melanchton Schule");
        daten.put("anschrift", "Adele-Sandrock-Strasse 75");
        daten.put("ort", "12627 Berlin");
        daten.put("homepage", "");
        daten.put("telefonnr", "0160 5978377");
        daten.put("breitengrad", 52.53487);
        daten.put("laengengrad", 13.61829);
        id = db.insert("orte", null, daten);
        Log.d("id des datensatzes: ", Long.toString(id));
        daten.clear();
        daten.put("id", 2);
        daten.put("art", "Familienzentrum");
        daten.put("region", "Hellersdorf-Ost / Mahlsdorf");
        daten.put("einrichtung", "Schuelerzentrum");
        daten.put("anschrift", "Adorfer Straße 6");
        daten.put("ort", "12627 Berlin");
        daten.put("homepage", "www.verbundev.de/?site=kraftwerk");
        daten.put("telefonnr", "(030) 9989731");
        daten.put("breitengrad", 52.53212);
        daten.put("laengengrad", 13.60678);
        id = db.insert("orte", null, daten);
        Log.d("id des datensatzes: ", Long.toString(id));
        daten.clear();
        daten.put("id", 3);
        daten.put("art", "Kindertagesstätte");
        daten.put("region", "Marzahn-Süd/ Biesdorf");
        daten.put("einrichtung", "Kita Wuhlespatzen");
        daten.put("anschrift", "Zum Forsthaus 3");
        daten.put("ort", "12683 Berlin");
        daten.put("homepage", "www.wuhlespatzen.de");
        daten.put("telefonnr", "(030) 5417075");
        daten.put("breitengrad", 52.5235710);
        daten.put("laengengrad", 13.5726430);
        id = db.insert("orte", null, daten);
        Log.d("id des datensatzes: ", Long.toString(id));
        daten.clear();
        daten.put("id", 4);
        daten.put("art", "Kindertagesstätte");
        daten.put("region", "Marzahn-Mitte");
        daten.put("einrichtung", "Kita Familienhaus Felix");
        daten.put("anschrift", "Zühlsdorfer Straße 16/ 18");
        daten.put("ort", "12679 Berlin");
        daten.put("homepage", "www.humanistischekitas.de/familienhaus-felix");
        daten.put("telefonnr", "(030) 9358035");
        daten.put("breitengrad", 52.5473000);
        daten.put("laengengrad", 13.5479700);
        id = db.insert("orte", null, daten);
        Log.d("id des datensatzes: ", Long.toString(id));
        daten.clear();
        daten.put("id", 5);
        daten.put("art", "Leseorte");
        daten.put("region", "Marzahn-Süd/ Biesdorf");
        daten.put("einrichtung", "Unfallkrankenhaus Berlin");
        daten.put("anschrift", "Warenstraße 7");
        daten.put("ort", "12683 Berlin");
        daten.put("homepage", "www.ukb.de");
        daten.put("telefonnr", "(030) 56811308");
        daten.put("breitengrad", 52.5194380);
        daten.put("laengengrad", 13.5672540);
        id = db.insert("orte", null, daten);
        Log.d("id des datensatzes: ", Long.toString(id));
        daten.clear();
        daten.put("id", 6);
        daten.put("art", "Jugendeinrichtung");
        daten.put("region", "Hellersdorf-Nord");
        daten.put("einrichtung", "Die Arche");
        daten.put("anschrift", "Tangermünder Straße 7");
        daten.put("ort", "12627 Berlin");
        daten.put("homepage", "www.kinderprojekt-arche.de");
        daten.put("telefonnr", "(030) 992888800");
        daten.put("breitengrad", 52.5445760);
        daten.put("laengengrad", 13.6057960);
        id = db.insert("orte", null, daten);
        Log.d("info id datensatz: ", Long.toString(id));
        daten.clear();
        Log.d("info", "abgespeichert");


     }




    @Override
    public synchronized void close() {
        if (db!=null) {
            db.close();
            db = null;
         }
        super.close();
     }

    public void delete() {
        boolean suc;
        suc = db.deleteDatabase(file);
        Log.d("info", Boolean.toString(suc));
     }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)  {

     }

    public void generateObjectInstances(String queryFilter) {
        cursor = db.query("orte", null, queryFilter, null, null, null, null);
        int anzahl = cursor.getCount();
        orte = new Ort[anzahl];
        cursor.moveToFirst();
        for(int i = 0; i < anzahl; i++) {
        orte[i] = new Ort();
        orte[i].setId(cursor.getInt(0));
        orte[i].setArt(cursor.getString(1));
        orte[i].setRegion(cursor.getString(2));
        orte[i].setEinrichtung(cursor.getString(3));
        orte[i].setanschrift(cursor.getString(4));
        orte[i].setOrt(cursor.getString(5));
        orte[i].setHomePage(cursor.getString(6));
        orte[i].setTelefonNr(cursor.getString(7));
        orte[i].setBreitenGrad(cursor.getDouble(8));
        orte[i].setLaengenGrad(cursor.getDouble(9));
            cursor.moveToNext();
         }
        cursor.close();
        //return orte;
     }

    public int getDbLenght(String queryFilter) {
        cursor = db.query("orte", null, queryFilter, null, null, null, null);
        int anzahl = cursor.getCount();
        cursor.close();
        return anzahl;
     }


    public void showData() {
        cursor = db.query("orte", null, null, null, null, null, null);
        int anzahl = cursor.getCount();
        if (anzahl < 1) {
            insertDemoData();
            cursor = db.query("orte", null, null, null, null, null, null);
            anzahl = cursor.getCount();
         }
        cursor.moveToFirst();
        for(int i= 0; i<anzahl; i++) {
            Log.d("info", cursor.getString(1));
            cursor.moveToNext();

         }
        cursor.close();
     }
 }
