package com.example.kevin.projekttms;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Switch;


/**
 * Created by kevin on 01.05.16.
 */
public class FragmentSettings extends Fragment implements View.OnClickListener {
    private Button btnBackToMenu;
    public static String TAG = "Fragmentanzeige";
    private Intent intent;
    private Switch switch1;
    private Switch switch2;
    private Switch switch3;
    private Switch switch4;
    private ConfigurationSingleton config = ConfigurationSingleton.getInstance();

    public FragmentSettings() {

     }

    public void onResume() {
        super.onResume();
     }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
     }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)  {
        super.onViewCreated(view, savedInstanceState);
         switch1 = (Switch)view.findViewById(R.id.switch1);
        switch1.setOnClickListener(this);
        switch1.setChecked(config.isLeseorte());
        switch2 = (Switch)view.findViewById(R.id.switch2);
        switch2.setOnClickListener(this);
        switch2.setChecked(config.isKindertagesstaette());
        switch3 = (Switch)view.findViewById(R.id.switch3);
        switch3.setOnClickListener(this);
        switch3.setChecked(config.isFamilienzentren());
        switch4 = (Switch)view.findViewById(R.id.switch4);
        switch4.setOnClickListener(this);
        switch4.setChecked(config.isJugendeinrichtungen());

     }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.content_fragment, container, false);

        return inflater.inflate(R.layout.content_fragment, container, false);

     }




    public void aktualisieren() {
        Activity activity = getActivity();
        activity.findViewById(R.id.switch1);
     }


    @Override
    public void onClick(View v)  {
        RefreshCallbackSwitch callback = (RefreshCallbackSwitch)getActivity();
        if (v == switch1) {
            Log.d("info", Boolean.toString(switch1.isChecked()));
            config.setLeseorte(switch1.isChecked());
         }  else  if (v == switch2) {
            Log.d("info", Boolean.toString(switch2.isChecked()));
            config.setKindertagesstaette(switch2.isChecked());
         }  else  if (v == switch3) {
            Log.d("info", Boolean.toString(switch3.isChecked()));
            config.setFamilienzentren(switch3.isChecked());
         }  else  if (v == switch4) {
            Log.d("info", Boolean.toString(switch4.isChecked()));
            config.setJugendeinrichtungen(switch4.isChecked());
         }  else
            Log.d("info", "nichts passiert");

        callback.refresh();
     }

    public interface RefreshCallbackSwitch {
        public void refresh();
     }


 }
