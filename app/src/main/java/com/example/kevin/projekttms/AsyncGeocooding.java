package com.example.kevin.projekttms;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.util.List;

/**
 * Created by kevin on 24.04.16.
 */
public class AsyncGeocooding extends AsyncTask  {
    private Context context;
    private Geocoder geocoder;
    private Ort[] orte;
    private List<Address> addressList;
    private DataBase dataBase;
    private Intent intent;


    public AsyncGeocooding(Context context, Ort[] orte) {
        this.context = context;
        geocoder = new Geocoder(context);
        this.orte = orte;

     }

    @Override
    protected Object doInBackground(Object[] params)  {
        for (int i = 0; i < orte.length; i++) {
            try  {
                addressList = geocoder.getFromLocationName(orte[i].getanschrift(), 1);
             }  catch (IOException e)  {
                e.printStackTrace();
             }
            if (addressList.size() == 0) {

             }  else  {
                Log.d("info" , String.valueOf(addressList.get(0)));
                orte[i].setBreitenGrad(addressList.get(0).getLatitude());
                orte[i].setLaengenGrad(addressList.get(0).getLongitude()); }
         }

        dataBase = new DataBase(context, "daten");
        dataBase.insertData(orte);

        Log.d("info", "finished tasks");

        return null;
     }
 }
