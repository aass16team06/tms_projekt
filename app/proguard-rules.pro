# Add project specif ic ProGuard rules here.
# By default, the flags in this file are appended to flags specif ied
# in /home/kevin/Android/Sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specif ic keep options here:

# if  your project uses WebView with JS, uncomment the following
# and specif y the fully qualif ied class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
